export interface MockTaskDecisionResourceConfig {
  hasMore?: boolean;
  lag?: number;
  error?: boolean;
  empty?: boolean;
}
