// Copy this file to local-config.js and customise.
exports.default = {
  serviceConfig: {
    url: 'http://example.com/',
  },
  initialQuery: {
    containerAri: 'example',
    sortCriteria: 'lastUpdateDate',
  },
};
