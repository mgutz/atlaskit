const path = require('path');

module.exports = [
  { name: 'Button', src: path.join(__dirname, '../src/components/Button.jsx') },
  { name: 'ButtonGroup', src: path.join(__dirname, '../src/components/ButtonGroup.jsx') },
];
