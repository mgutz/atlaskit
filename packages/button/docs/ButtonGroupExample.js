import React from 'react';
import Button, { ButtonGroup } from '@atlaskit/button';

const ButtonGroupExample = () => (
  <ButtonGroup>
    <Button>
      First Button
    </Button>
    <Button>
      Second Button
    </Button>
    <Button>
      Button Tertius
    </Button>
    <Button>
      Fourth Button
    </Button>
  </ButtonGroup>
);

export default ButtonGroupExample;
