const path = require('path');

module.exports = [
  { name: 'Tooltip', src: path.join(__dirname, '../src/components/Tooltip.jsx') },
  { name: 'ToolTip Stateless', src: path.join(__dirname, '../src/components/TooltipStateless.jsx') },
];
