export type tagColor = 'standard' | 'green' | 'blue' | 'red' | 'purple' | 'grey' | 'teal' | 'yellow' | 'greenLight' | 'blueLight' | 'redLight' | 'purpleLight' | 'greyLight' | 'tealLight' | 'yellowLight';

export type appearanceType = 'default' | 'rounded';

export type ReactElement = any;
