import { gridSize } from '@atlaskit/theme';

export const defaultMaxItems = 8;
export const itemTruncateWidth = gridSize() * 25;
