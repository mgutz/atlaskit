/*
Because each SVG logo's type (the text part after the icon) is offset from the left SVG edge
at an unpredictable distance, we need to define the distance here.
*/

// 1st value is horizontal type offset
// 2nd value is raw SVG height
export const atlassianTypeOffset = 43 / 32;
export const bitbucketTypeOffset = 42 / 32;
export const confluenceTypeOffset = 42 / 32;
export const hipchatTypeOffset = 42 / 32;
export const jiraTypeOffset = 42 / 32;
export const jiraCoreTypeOffset = 37 / 32;
export const jiraServiceDeskTypeOffset = 36 / 32;
export const jiraSoftwareTypeOffset = 41 / 32;
export const statuspageTypeOffset = 42 / 32;
export const strideTypeOffset = 42 / 32;
