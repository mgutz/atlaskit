import {
  AtlassianLogo,
  BitbucketLogo,
  ConfluenceLogo,
  HipchatLogo,
  JiraLogo,
  JiraCoreLogo,
  JiraServiceDeskLogo,
  JiraSoftwareLogo,
  StatuspageLogo,
  StrideLogo,
} from './components/Logos';

export default AtlassianLogo;
export {
  AtlassianLogo,
  BitbucketLogo,
  ConfluenceLogo,
  HipchatLogo,
  JiraLogo,
  JiraCoreLogo,
  JiraServiceDeskLogo,
  JiraSoftwareLogo,
  StatuspageLogo,
  StrideLogo,
};
