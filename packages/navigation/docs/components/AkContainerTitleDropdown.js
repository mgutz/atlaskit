import React from 'react';

export const description = (
  <div>
    This component is designed to render an AkContainerTitle as a dropdown trigger.
    Children need to be DropdownItems.
  </div>
);

export const examples = [];

export const byline = 'Component to be provided to select or change container.';
