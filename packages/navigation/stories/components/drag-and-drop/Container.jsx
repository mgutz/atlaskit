import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  `;

Container.displayName = 'Container';

export default Container;
