import styled from 'styled-components';

import { colors } from '@atlaskit/theme';

const WhereToFindNewProductText = styled.p`color: ${colors.N300};`;

WhereToFindNewProductText.displayName = 'WhereToFindNewProductText';
export default WhereToFindNewProductText;
