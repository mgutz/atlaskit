import styled from 'styled-components';

const RequestTrialHeading = styled.h3`
  margin-top: 0px;
`;

RequestTrialHeading.displayName = 'RequestTrialHeading';
export default RequestTrialHeading;
