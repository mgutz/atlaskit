const path = require('path');

module.exports = [
  { name: 'Calendar', src: path.join(__dirname, '../src/components/Calendar.jsx') },
  { name: 'CalendarStateless', src: path.join(__dirname, '../src/components/CalendarStateless.jsx') },
];
