# Progress Indicator

## Dots

The progress dots are visual indicators used when stepping a user through a
journey, to allow them to keep track of their progress.

They are typically accompanied by a carousel or other such UI device.

## Try it out

Interact with a [live demo of the @NAME@ component](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).

## Installation

```sh
npm install @NAME@
```
