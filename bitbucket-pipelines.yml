# make sure not to use && as a continuation in any of these steps unless you pair it with || exit 1
# pipelines concats each of these commands into one script and runs that
# if your command in the script list has && joining it, and any of the steps fail, the pipeline will still continue
#
# please either put those commands in a script in ./build/bin
# or just place them in different items in the list i.e. change
#           - foo && bar && baz
# to
#           - foo
#           - bar
#           - baz
# or
#           - foo && bar && baz || exit 1
# or
#           - ./build/bin/foo-bar-baz.bash

image: atlassianlabs/atlaskit:2017-05-10
pipelines:
  branches:
    master:
      - step:
          script:
            - export FORCE_COLOR=1
            - ./build/bin/setup.sh
            - ./build/bin/prep_for_release.sh
            - yarn
            - ./build/bin/release.stop.pipeline.if.other.master.running.js
            - run-p -ln "validate/package.json" "validate/typescript"
            - run-p -ln "validate/tslint" "validate/eslint"
            # --runInBand is used to make jest run in serial rather than parallel to prevent OOM
            # --bail is used to make jest fail fast when it finds a test failure
            - npm run test:unit -- --runInBand --bail
            - npm run test:base
            - yarn run browserstack/tunnel/start
            - yarn run test:browserstack
            - yarn run browserstack/tunnel/stop
            - yarn run release/check/is-workspace-clean --silent
            - yarn run release/check/can-ff --silent || exit 0
            - npm run release
            - run-p -ln "release/registry" "release/storybooks"
            - yarn run release/announce
            - ./build/bin/release.restart.last.stopped.pipeline.js
          # caches:
            # - node
    invalidate-atlaskit-cache:
      - step:
          script:
            - ./build/bin/cloudfront_invalidate.sh
  default:
    - step:
        script:
          - export FORCE_COLOR=1
          - export CURRENT_BUILD_TIME=$(date +'%Y-%m-%d_%H_%m_%S')
          - ./build/bin/setup.sh
          # need to run this so we have access to the Bitbucket credentials during generate.changed.packages.file.pr.sh
          - ./build/bin/prep_for_release.sh
          - yarn
          # we have to run this after `yarn` to have access to child-process-promise
          - ./build/bin/generate.changed.packages.file.pr.sh
          # if you find yourself running into out of memory errors, we can move more steps out of this parallel step
          - run-p -ln "validate/package.json" "validate/typescript"
          - run-p -ln "validate/tslint" "validate/eslint"
          - yarn run browserstack/tunnel/start
          - yarn run test:browserstack
          - yarn run browserstack/tunnel/stop
          # --runInBand is used to make jest run in serial rather than parallel to prevent OOM
          # --bail is used to make jest fail fast when it finds a test failure
          - npm run test:unit -- --runInBand --bail
          - npm run test:base
          - yarn run "pr/storybook"
        # caches:
          # - node

  custom: # Pipelines that can be scheduled or triggered manually

    # We add "dev" to the name to make it come first alphabetically
    build-website-dev-staging:
      - step:
          script:
            - ./build/bin/setup.sh
            - yarn
            - cd packages/website
            - npm run deploy-staging

    # This build should only be called by the scheduled build. We are in a hard catch-22 though, to
    # make this build only be able to be run on a specific branch, we need to prevent being able to
    # to run it manually (prep_for_release.sh will fail as BITBUCKET_BRANCH will not be set)

    ##################################################################################################
    #  NOTE: If you are trying to change this script, you'll need to make sure you merge the changes #
    #        into `production-website-DO-NOT-USE` or they wont happen in the scheduled build         #
    ##################################################################################################
    build-website-production:
      - step:
          script:
            - ./build/bin/setup.sh
            # need to run this so we have access to the Bitbucket credentials so we can pull master
            - ./build/bin/prep_for_release.sh
            - ./build/bin/exit.if.branch.name.not.matches.js "production-website-DO-NOT-USE" || exit
            # we deliberately run this in a non-master branch so it doesnt clog up the pipelines ui
            # and block actual master builds
            - git fetch
            - git checkout origin/master
            - git pull
            - yarn
            - cd packages/website
            - npm run deploy

    run-browserstack: # this is to allow users to manually run BS for a commit
      - step:
          script:
            - export FORCE_BS_TESTS="Yes"
            - ./build/bin/setup.sh
            # required so that we can fetch origin
            - ./build/bin/prep_for_release.sh
            - yarn
            - yarn run browserstack/tunnel/start
            - yarn run test:browserstack
            - yarn run browserstack/tunnel/stop
